package com.spring.core.logger;

import java.util.Set;

import com.spring.core.event.Event;

public class CombineEventLogger implements EventLogger {
	private Set<EventLogger> eventLoggers;

	@Override
	public void logEvent(final Event msg) {
		for (final EventLogger eventLogger : getEventLoggers()) {
			eventLogger.logEvent(msg);
		}
	}

	public CombineEventLogger(final Set<EventLogger> eventLoggers) {
		super();
		this.eventLoggers = eventLoggers;
	}

	public Set<EventLogger> getEventLoggers() {
		return eventLoggers;
	}

	public void setEventLoggers(final Set<EventLogger> eventLoggers) {
		this.eventLoggers = eventLoggers;
	}

}
