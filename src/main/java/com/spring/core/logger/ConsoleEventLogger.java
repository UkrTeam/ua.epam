package com.spring.core.logger;

import com.spring.core.event.Event;

public class ConsoleEventLogger implements EventLogger {
	/* (non-Javadoc)
	 * @see com.spring.core.logger.EventLogger#logEvent(java.lang.String)
	 */
	public void logEvent(final Event msg){
		System.out.println(msg);
	}
}
