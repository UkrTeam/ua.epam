package com.spring.core.logger;

import java.util.ArrayList;
import java.util.List;

import com.spring.core.event.Event;

public class CacheFileEventLogger extends FileEventLogger {
	private int CacheSize;
	private final List<Event> cache;


	public void destroy(){
		if(!getCache().isEmpty()){
			for(final Event e: getCache()){
				super.logEvent(e);
			}
		}
	}

	@Override
	public void logEvent(final Event msg) {
		cache.add(msg);
		if(getCache().size()==getCacheSize()){
			super.logEvent(msg);
			cache.clear();
		}
	}

	public CacheFileEventLogger(final String fileName, final int cacheSize) {
		super(fileName);
		CacheSize = cacheSize;
		cache = new ArrayList<Event>(getCacheSize());
	}


	public List<Event> getCache() {
		return cache;
	}
	public int getCacheSize() {
		return CacheSize;
	}

	public void setCacheSize(final int cacheSize) {
		CacheSize = cacheSize;
	}

}
