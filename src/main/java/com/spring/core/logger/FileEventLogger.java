package com.spring.core.logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.spring.core.event.Event;

public class FileEventLogger implements EventLogger {

	private String fileName;

	@Override
	public void logEvent(final Event msg) {
		try {
			FileUtils.writeStringToFile(new File(getFileName()), msg.toString()
					+ "\n", true);
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}

	public void init() throws IOException {
		final File file = new File(getFileName());
		if ((!file.exists()) || (!file.canWrite())) {
			file.createNewFile();
		}
	}

	public FileEventLogger(final String fileName) {
		super();
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(final String fileName) {
		this.fileName = fileName;
	}

}
