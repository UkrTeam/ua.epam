package com.spring.core.logger;

import com.spring.core.event.Event;

public interface EventLogger {

	public abstract void logEvent(final Event msg);

}