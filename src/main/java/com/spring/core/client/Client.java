package com.spring.core.client;

public class Client {

	private int id;

	private String fullName;
	private String greating;



	public String getGreating() {
		return greating;
	}

	public void setGreating(final String greating) {
		this.greating = greating;
	}

	public Client(final int id, final String fullName) {
		super();
		this.id = id;
		this.fullName = fullName;
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(final String fullName) {
		this.fullName = fullName;
	}
}
