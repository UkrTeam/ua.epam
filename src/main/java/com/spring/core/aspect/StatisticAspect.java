package com.spring.core.aspect;

import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class StatisticAspect {

	private final Map<Class, Integer> counter = new HashMap();;

	@Before("execution(* *.logEvent(..))")
	public void count(final JoinPoint joinPoint) {
		final Class<?> clazz = joinPoint.getTarget().getClass();
		if (!counter.containsKey(clazz)) {
			counter.put(clazz, 0);
		}
		int count = counter.get(clazz);
		counter.put(clazz, ++count);
	}

	public Map<Class, Integer> getCounter() {
		return counter;
	}
}
