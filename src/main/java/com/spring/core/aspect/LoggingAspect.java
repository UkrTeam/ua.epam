package com.spring.core.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {

	@Before("execution(* *.logEvent(..))")
	public void logBefore(final JoinPoint joinpoint) {
		System.out.println("Hacked: " + joinpoint.getSignature().getName());
	}
}
