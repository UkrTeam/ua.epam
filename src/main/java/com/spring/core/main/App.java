package com.spring.core.main;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.core.aspect.StatisticAspect;
import com.spring.core.client.Client;
import com.spring.core.event.Event;
import com.spring.core.event.EventType;
import com.spring.core.logger.EventLogger;

public class App {
	@Autowired
	private Client client;
	private EventLogger defaultLogger;
	private Map<EventType, EventLogger> loggers;

	public Map<EventType, EventLogger> getLoggers() {
		return loggers;
	}

	public void setLoggers(final Map<EventType, EventLogger> loggers) {
		this.loggers = loggers;
	}

	public App(final EventLogger consoleEventLogger) {
		super();
		this.defaultLogger = consoleEventLogger;
	}

	public App() {
		super();
	}

	public void logEvent(final Event msg, final EventType eventType) {
		if (msg != null) {
			if (loggers.get(eventType) == null) {
				defaultLogger.logEvent(msg);
			} else {
				loggers.get(eventType).logEvent(msg);
			}
		}
	}

	public static void main(final String[] args) {
		final ConfigurableApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"spring.xml");
		final App app = (App) applicationContext.getBean("app");
		final Event event1 = (Event) applicationContext.getBean("event");
		final Event event2 = (Event) applicationContext.getBean("event");
		app.logEvent(event1, EventType.ERROR);
		app.logEvent(event2, EventType.INFO);
		app.logEvent(event2, null);

		final StatisticAspect staticAspect = (StatisticAspect) applicationContext
				.getBean("statisticAspect");
		final Map<Class, Integer> counter = staticAspect.getCounter();
		for (final Class c : counter.keySet()) {
			System.out.println(c.getSimpleName() + "   "
					+ staticAspect.getCounter().get(c));
		}
		applicationContext.close();
	}

	public Client getClient() {
		return client;
	}

	public void setClient(final Client client) {
		this.client = client;
	}

	public EventLogger getConsoleEventLogger() {
		return defaultLogger;
	}

	public void setConsoleEventLogger(final EventLogger consoleEventLogger) {
		this.defaultLogger = consoleEventLogger;
	}
}
