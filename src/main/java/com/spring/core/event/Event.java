package com.spring.core.event;

import java.text.DateFormat;
import java.util.Date;

public class Event {
	private int id;
	private String msg;
	private Date date;
	private EventType eventType;
	private final DateFormat dateFormat;

	private static int idGenerator;

	public Event(final Date date, final DateFormat df) {
		id = idGenerator++;
		this.date = date;
		dateFormat = df;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("id ");
		builder.append(getId());
		builder.append("msg ");
		builder.append(getMsg());
		builder.append("date ");
		builder.append(getDateFormat().format(getDate()));

		return builder.toString();
	}

	public DateFormat getDateFormat() {
		return dateFormat;
	}

	public int getId() {
		return id;
	}

	public void setId(final int id) {
		this.id = id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(final String msg) {
		this.msg = msg;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(final Date date) {
		this.date = date;
	}
}
