package com.spring.core.event;

public enum EventType {
	INFO, ERROR;
}
